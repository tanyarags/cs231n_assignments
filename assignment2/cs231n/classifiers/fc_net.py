import numpy as np

from cs231n.layers import *
from cs231n.layer_utils import *


class TwoLayerNet(object):
  """
  A two-layer fully-connected neural network with ReLU nonlinearity and
  softmax loss that uses a modular layer design. We assume an input dimension
  of D, a hidden dimension of H, and perform classification over C classes.
  
  The architecure should be affine - relu - affine - softmax.

  Note that this class does not implement gradient descent; instead, it
  will interact with a separate Solver object that is responsible for running
  optimization.

  The learnable parameters of the model are stored in the dictionary
  self.params that maps parameter names to numpy arrays.
  """
  
  def __init__(self, input_dim=3*32*32, hidden_dim=100, num_classes=10,
               weight_scale=1e-3, reg=0.0):
    """
    Initialize a new network.

    Inputs:
    - input_dim: An integer giving the size of the input
    - hidden_dim: An integer giving the size of the hidden layer
    - num_classes: An integer giving the number of classes to classify
    - dropout: Scalar between 0 and 1 giving dropout strength.
    - weight_scale: Scalar giving the standard deviation for random
      initialization of the weights.
    - reg: Scalar giving L2 regularization strength.
    """
    self.params = {}
    self.reg = reg
    
    ############################################################################
    # TODO: Initialize the weights and biases of the two-layer net. Weights    #
    # should be initialized from a Gaussian with standard deviation equal to   #
    # weight_scale, and biases should be initialized to zero. All weights and  #
    # biases should be stored in the dictionary self.params, with first layer  #
    # weights and biases using the keys 'W1' and 'b1' and second layer weights #
    # and biases using the keys 'W2' and 'b2'.                                 #
    ############################################################################
    size_b1 = (hidden_dim, 1)
    size_b2 = (num_classes, 1)
    self.params['W1'] = weight_scale * np.random.randn(input_dim, hidden_dim)
    self.params['W2'] = weight_scale * np.random.randn(hidden_dim, num_classes)
    self.params['b1'] = np.zeros(size_b1)
    self.params['b2'] = np.zeros(size_b2)
    
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################


  def loss(self, X, y=None):
    """
    Compute loss and gradient for a minibatch of data.

    Inputs:
    - X: Array of input data of shape (N, d_1, ..., d_k)
    - y: Array of labels, of shape (N,). y[i] gives the label for X[i].

    Returns:
    If y is None, then run a test-time forward pass of the model and return:
    - scores: Array of shape (N, C) giving classification scores, where
      scores[i, c] is the classification score for X[i] and class c.

    If y is not None, then run a training-time forward and backward pass and
    return a tuple of:
    - loss: Scalar value giving the loss
    - grads: Dictionary with the same keys as self.params, mapping parameter
      names to gradients of the loss with respect to those parameters.
    """  
    scores = None
    ############################################################################
    # TODO: Implement the forward pass for the two-layer net, computing the    #
    # class scores for X and storing them in the scores variable.              #
    ############################################################################
    
    W1 = self.params['W1']
    b1 = self.params['b1']
    out_AfRelu, cache_AfRelu = affine_relu_forward(X, W1, b1)
    
    W2 = self.params['W2']
    b2 = self.params['b2']
    scores, cache_AfSoftmax = affine_forward(out_AfRelu, W2, b2)
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # If y is None then we are in test mode so just return scores
    if y is None:
      return scores
    
    loss, grads = 0, {}
    ############################################################################
    # TODO: Implement the backward pass for the two-layer net. Store the loss  #
    # in the loss variable and gradients in the grads dictionary. Compute data #
    # loss using softmax, and make sure that grads[k] holds the gradients for  #
    # self.params[k]. Don't forget to add L2 regularization!                   #
    #                                                                          #
    # NOTE: To ensure that your implementation matches ours and you pass the   #
    # automated tests, make sure that your L2 regularization includes a factor #
    # of 0.5 to simplify the expression for the gradient.                      #
    ############################################################################
    #loss calculation
    
    num_classes = W2.shape[1]
    num_train = X.shape[0]
    reg = self.reg
    
    #right score matrix
    score_mask = np.arange(num_train*num_classes).reshape(scores.shape)
    score_mask = np.mod(score_mask, num_classes) - np.array(y).reshape(-1, 1)
    
    mask_right_class = np.array(score_mask==0)
    mask_wrong_class = np.array(score_mask!=0)  
    
    scores_normalized = np.subtract(scores,np.max(scores, axis = 1).reshape(-1,1))    
    probs = np.exp(scores_normalized) / (np.sum(np.exp(scores_normalized), axis = 1)).reshape(-1,1)
    log_prob = (-np.log(probs)) * mask_right_class
    loss = np.sum(log_prob)    
    loss /= num_train
    
    #--L2 Regularization Loss--#
    loss = loss + (0.5)*reg*(sum(sum(W1*W1)) + sum(sum(W2*W2))  )
    
    #Gradient calculation:
    #initial deltas
    dx_softmax = mask_right_class*(probs - 1) +  mask_wrong_class*(probs) #since last layer
    
    #backward pass- affine
    dx_afSoftmax, dW2, db2 = affine_backward(dx_softmax, cache_AfSoftmax)
    dx, dW1, db1 = affine_relu_backward(dx_afSoftmax, cache_AfRelu)
    
    dW1 /= num_train
    dW2 /= num_train
    db1 /= num_train
    db2 /= num_train
    
    #gradient effect due to regularization
    #add the regularization loss for both weights#
    dW2 = np.add(dW2, reg*W2)#regularization loss#   
    dW1 = np.add(dW1, reg*W1)#regularization loss#
        
    #updating the gradients
    grads['W1'] = dW1
    grads['W2'] = dW2
    grads['b1'] = db1
    grads['b2'] = db2
    
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads


class FullyConnectedNet(object):
  """
  A fully-connected neural network with an arbitrary number of hidden layers,
  ReLU nonlinearities, and a softmax loss function. This will also implement
  dropout and batch normalization as options. For a network with L layers,
  the architecture will be
  
  {affine - [batch norm] - relu - [dropout]} x (L - 1) - affine - softmax
  
  where batch normalization and dropout are optional, and the {...} block is
  repeated L - 1 times.
  
  Similar to the TwoLayerNet above, learnable parameters are stored in the
  self.params dictionary and will be learned using the Solver class.
  """

  def __init__(self, hidden_dims, input_dim=3*32*32, num_classes=10,
               dropout=0, use_batchnorm=False, reg=0.0,
               weight_scale=1e-2, dtype=np.float32, seed=None):
    """
    Initialize a new FullyConnectedNet.
    
    Inputs:
    - hidden_dims: A list of integers giving the size of each hidden layer.
    - input_dim: An integer giving the size of the input.
    - num_classes: An integer giving the number of classes to classify.
    - dropout: Scalar between 0 and 1 giving dropout strength. If dropout=0 then
      the network should not use dropout at all.
    - use_batchnorm: Whether or not the network should use batch normalization.
    - reg: Scalar giving L2 regularization strength.
    - weight_scale: Scalar giving the standard deviation for random
      initialization of the weights.
    - dtype: A numpy datatype object; all computations will be performed using
      this datatype. float32 is faster but less accurate, so you should use
      float64 for numeric gradient checking.
    - seed: If not None, then pass this random seed to the dropout layers. This
      will make the dropout layers deteriminstic so we can gradient check the
      model.
    """
    self.use_batchnorm = use_batchnorm
    self.use_dropout = dropout > 0
    self.reg = reg
    self.num_layers = 1 + len(hidden_dims)
    self.dtype = dtype
    self.params = {}

    ############################################################################
    # TODO: Initialize the parameters of the network, storing all values in    #
    # the self.params dictionary. Store weights and biases for the first layer #
    # in W1 and b1; for the second layer use W2 and b2, etc. Weights should be #
    # initialized from a normal distribution with standard deviation equal to  #
    # weight_scale and biases should be initialized to zero.                   #
    #                                                                          #
    # When using batch normalization, store scale and shift parameters for the #
    # first layer in gamma1 and beta1; for the second layer use gamma2 and     #
    # beta2, etc. Scale parameters should be initialized to one and shift      #
    # parameters should be initialized to zero.                                #
    ############################################################################
       
    #first layer
    size_b1 = (hidden_dims[0],1)
    self.params['W1'] = weight_scale * np.random.randn(input_dim, hidden_dims[0])
    self.params['b1'] = np.zeros(size_b1)
    
    #following hidden layers, excluding the last layer
    for hid_num in range(self.num_layers - 2):
        size_bi = (hidden_dims[hid_num+1], 1)
        self.params['W' + str(hid_num + 2)] = weight_scale * np.random.randn(hidden_dims[hid_num], hidden_dims[hid_num+1])
        self.params['b' + str(hid_num + 2)] = np.zeros(size_bi)
    
    size_b_last = (num_classes, 1)
    self.params['W' + str(self.num_layers)] = weight_scale * np.random.randn(hidden_dims[self.num_layers -2], num_classes)
    self.params['b' + str(self.num_layers)] = np.zeros(size_b_last)
    
    
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # When using dropout we need to pass a dropout_param dictionary to each
    # dropout layer so that the layer knows the dropout probability and the mode
    # (train / test). You can pass the same dropout_param to each dropout layer.
    self.dropout_param = {}
    if self.use_dropout:
      self.dropout_param = {'mode': 'train', 'p': dropout}
      if seed is not None:
        self.dropout_param['seed'] = seed
    
    # With batch normalization we need to keep track of running means and
    # variances, so we need to pass a special bn_param object to each batch
    # normalization layer. You should pass self.bn_params[0] to the forward pass
    # of the first batch normalization layer, self.bn_params[1] to the forward
    # pass of the second batch normalization layer, etc.
    self.bn_params = []
    if self.use_batchnorm:
      self.bn_params = [{'mode': 'train'} for i in xrange(self.num_layers - 1)]
    
    # Cast all parameters to the correct datatype
    for k, v in self.params.iteritems():
      self.params[k] = v.astype(dtype)


  def loss(self, X, y=None):
    """
    Compute loss and gradient for the fully-connected net.

    Input / output: Same as TwoLayerNet above.
    """
    X = X.astype(self.dtype)
    mode = 'test' if y is None else 'train'

    # Set train/test mode for batchnorm params and dropout param since they
    # behave differently during training and testing.
    if self.dropout_param is not None:
      self.dropout_param['mode'] = mode   
    if self.use_batchnorm:
      for bn_param in self.bn_params:
        bn_param[mode] = mode

    scores = None
    ############################################################################
    # TODO: Implement the forward pass for the fully-connected net, computing  #
    # the class scores for X and storing them in the scores variable.          #
    #                                                                          #
    # When using dropout, you'll need to pass self.dropout_param to each       #
    # dropout forward pass.                                                    #
    #                                                                          #
    # When using batch normalization, you'll need to pass self.bn_params[0] to #
    # the forward pass for the first batch normalization layer, pass           #
    # self.bn_params[1] to the forward pass for the second batch normalization #
    # layer, etc.                                                              #
    ############################################################################
    
    #cache and outputs of each layer.
    caches = {}
    outputs = {}

    in_AfRelu = X
    
    #first L-1 layers
    for hid_num in range(self.num_layers - 1):
        W_i = self.params['W' + str(hid_num + 1)]
        b_i = self.params['b' + str(hid_num + 1)]
        outputs['L' + str(hid_num + 1)], caches['L' + str(hid_num + 1)] = affine_relu_forward(in_AfRelu, W_i, b_i)
        in_AfRelu = outputs['L' + str(hid_num + 1)]
    
    W_last = self.params['W' + str(self.num_layers)]
    b_last = self.params['b' + str(self.num_layers)]
    outputs['L' + str(self.num_layers)], caches['L' + str(self.num_layers)] = affine_forward(in_AfRelu, W_last, b_last)
    scores = outputs['L' + str(self.num_layers)]
    
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    # If test mode return early
    if mode == 'test':
      return scores

    loss, grads = 0.0, {}
    ############################################################################
    # TODO: Implement the backward pass for the fully-connected net. Store the #
    # loss in the loss variable and gradients in the grads dictionary. Compute #
    # data loss using softmax, and make sure that grads[k] holds the gradients #
    # for self.params[k]. Don't forget to add L2 regularization!               #
    #                                                                          #
    # When using batch normalization, you don't need to regularize the scale   #
    # and shift parameters.                                                    #
    #                                                                          #
    # NOTE: To ensure that your implementation matches ours and you pass the   #
    # automated tests, make sure that your L2 regularization includes a factor #
    # of 0.5 to simplify the expression for the gradient.                      #
    ############################################################################
    
    
    W_last = self.params['W' + str(self.num_layers)]
    num_classes = W_last.shape[1]
    num_train = X.shape[0]
    reg = self.reg
    
    #right score matrix
    score_mask = np.arange(num_train*num_classes).reshape(scores.shape)
    score_mask = np.mod(score_mask, num_classes) - np.array(y).reshape(-1, 1)
    
    mask_right_class = np.array(score_mask==0)
    mask_wrong_class = np.array(score_mask!=0)  
    
    scores_normalized = np.subtract(scores,np.max(scores, axis = 1).reshape(-1,1))    
    probs = np.exp(scores_normalized) / (np.sum(np.exp(scores_normalized), axis = 1)).reshape(-1,1)
    log_prob = (-np.log(probs)) * mask_right_class
    loss = np.sum(log_prob)    
    loss /= num_train
    
    #--L2 Regularization Loss--#
    for w_iter in range(self.num_layers):
        W = self.params['W' + str(w_iter + 1)]
        loss+=  (0.5)*self.reg*(sum(sum(W*W)))
        
    #Gradient calculation:
    #initial deltas
    dx_softmax = mask_right_class*(probs - 1) +  mask_wrong_class*(probs) #since last layer
                                 
    dx = {}
    dW = {}
    db = {}
    #backward pass- affine
    dx['L' + str(self.num_layers)] = dx_softmax                             
    for w_iter in range(self.num_layers, 0, -1):
        layer_tag = 'L' + str(w_iter)                         
        layer_before = 'L' + str(w_iter - 1)
        if w_iter == self.num_layers:
            dx[layer_before], dW[layer_tag], db[layer_tag] = affine_backward(dx[layer_tag], caches[layer_tag])
                                 
        else:                            
            dx[layer_before], dW[layer_tag], db[layer_tag] = affine_relu_backward(dx[layer_tag], caches[layer_tag])
    
    for w_iter in range(self.num_layers, 0, -1):
    
        #gradient effect due to regularization
        #add the regularization loss for both weights#
        layer_tag = 'L' + str(w_iter)
        W_tag = 'W' + str(w_iter)
        b_tag = 'b' + str(w_iter)
        dW_iter = dW[layer_tag]/ num_train
        db_iter = db[layer_tag]/ num_train
        W_iter = self.params[W_tag]
        
        dW_iter = np.add(dW_iter, reg*W_iter)#regularization loss#
        #updating the gradients
        grads[W_tag] = dW_iter   
        grads[b_tag] = db_iter
     
        
    ############################################################################
    #                             END OF YOUR CODE                             #
    ############################################################################

    return loss, grads

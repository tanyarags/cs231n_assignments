import numpy as np
from random import shuffle

def svm_loss_naive(W, X, y, reg):
  """
  Structured SVM loss function, naive implementation (with loops).

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  dW = np.zeros(W.shape) # initialize the gradient as zero

  # compute the loss and the gradient
  num_classes = W.shape[1]
  num_train = X.shape[0]
  loss = 0.0
  for i in xrange(num_train):
    
    scores = X[i].dot(W)
    correct_class_score = scores[y[i]]
    
    for j in xrange(num_classes):
      if j == y[i]:
        continue
      
      margin = scores[j] - correct_class_score + 1 # note delta = 1
      if margin > 0:
        dW[:,y[i]] = dW[:,y[i]] - np.transpose(X[i]) # note delta = 1
        dW[:,j] = dW[:,j] + np.transpose(X[i]) # note delta = 1
        loss += margin

  # Right now the loss is a sum over all training examples, but we want it
  # to be an average instead so we divide by num_train.
  loss /= num_train
  dW /= num_train

  # Add regularization to loss and gradient
  loss += 0.5 * reg * np.sum(W * W)
  dW = np.add(dW, reg*W)
    

  #############################################################################
  # TODO:                                                                     #
  # Compute the gradient of the loss function and store it dW.                #
  # Rather that first computing the loss and then computing the derivative,   #
  # it may be simpler to compute the derivative at the same time that the     #
  # loss is being computed. As a result you may need to modify some of the    #
  # code above to compute the gradient.                                       #
  #############################################################################


  return loss, dW


def svm_loss_vectorized(W, X, y, reg):
  """
  Structured SVM loss function, vectorized implementation.

  Inputs and outputs are the same as svm_loss_naive.
  """
  loss = 0.0
  dW = np.zeros(W.shape) # initialize the gradient as zero

  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the structured SVM loss, storing the    #
  # result in loss.                                                           #
  #############################################################################
  delta = 1
  if X is None:
    print 'error handling None'    
    return loss, dW

  num_train = X.shape[0]
  scores = X.dot(W)
  num_classes = scores.shape[1]  
  
  # compute the margins for all classes in one vector operation
  score_mask = np.arange(scores.shape[0]*scores.shape[1]).reshape(scores.shape)
  score_mask = np.mod(score_mask, num_classes)
  score_mask = score_mask - np.array(y).reshape(-1, 1)
  mask_right_class = np.array(score_mask==0)
  mask_wrong_class = np.array(score_mask!=0)  
   
  masked_score = np.extract(mask_right_class, scores).reshape(-1,1)  
  #print scores[0,0],masked_score[0,0],y[0], xa[0,0]
  margins = np.maximum(0.0, scores - masked_score + 1)
  masked_wrong_margins = mask_wrong_class * margins
        
  #for calculating gradient. 
  #calculate for each X[i] the number of wrong classes where score >0
  wrong_class_flag = np.array(masked_wrong_margins > 0)  
  num_wrong_class = -1*np.sum(wrong_class_flag, axis=1).reshape(-1,1)

  mask_dW = np.zeros((num_train,num_classes))
  mask_dW = mask_right_class*(num_wrong_class)  
  mask_dW = mask_dW + wrong_class_flag
    
  dW = np.dot(np.transpose(X),mask_dW)
  # on y-th position scores[y] - scores[y] canceled and gave delta. We want
  # to ignore the y-th position and only consider margin on max wrong class
    
  loss = np.sum(masked_wrong_margins)
  
  loss /= num_train
  dW /= num_train
  # Add regularization to the loss.
  loss += 0.5 * reg * np.sum(W * W)  
  dW = np.add(dW, reg*W)  
  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################


  #############################################################################
  # TODO:                                                                     #
  # Implement a vectorized version of the gradient for the structured SVM     #
  # loss, storing the result in dW.                                           #
  #                                                                           #
  # Hint: Instead of computing the gradient from scratch, it may be easier    #
  # to reuse some of the intermediate values that you used to compute the     #
  # loss.                                                                     #
  #############################################################################
  
  
  
  #############################################################################
  #                             END OF YOUR CODE                              #
  #############################################################################

  return loss, dW
